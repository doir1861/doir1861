package salariati.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class ModifyEmployeeTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;


    @Before
    public void setUp() throws Exception {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void modifyEmployee() {
        //Test 1
        controller.clearRepo();

        Employee Marin = new Employee("Marin Puscas", "1234567890876", DidacticFunction.CONFERENTIAR, "2500");
        controller.addEmployee(Marin);
        Employee newEmployee1 = new Employee("Octavian", "196042805508", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Marin, newEmployee1);
        assertTrue(controller.getEmployeesList().get(0).getCnp().equals("1234567890876"));

        //Test 2
        Employee Mihai   = new Employee("Mihai Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "2500");
        Employee Ionela  = new Employee("Ionela Ionescu", "1224567890876", DidacticFunction.LECTURER, "2500");
        controller.addEmployee(Mihai);
        controller.addEmployee(Ionela);
        Employee newEmployee2 = new Employee("Octavian", "196042805508", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Mihai, newEmployee2);
        assertTrue(controller.getEmployeesList().get(1).getCnp().equals("1234567890876"));


        //Test 3
        Employee newEmployee3 = new Employee("Octavian", "1960428055085", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(Mihai, newEmployee3);
        assertTrue(controller.getEmployeesList().get(1).getCnp().equals("1960428055085"));

        //Test 4
        Employee notFoundEmployee = new Employee("Barbu", "1960428055083", DidacticFunction.ASISTENT, "2000");
        Employee newEmployee4 = new Employee("Octavian", "1960428054085", DidacticFunction.ASISTENT, "2000");
        controller.modifyEmployee(notFoundEmployee, newEmployee4);
        assertTrue(controller.getEmployeesList().size() == 2);
    }
}