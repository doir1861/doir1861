package salariati.validator;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isNameValid = employee.getName().matches("[a-zA-Z ]+") && (employee.getName().length() > 2);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER) ||
				                   employee.getFunction().equals(DidacticFunction.CONFERENTIAR);
		boolean isSalaryValid    = employee.getSalary().matches("[0-9]+") && (employee.getSalary().length() > 1) && (Float.parseFloat(employee.getSalary()) > 0);
		
		return isNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}
}
