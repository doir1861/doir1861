package salariati.repository.implementations;

import java.io.*;
import java.util.*;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "src/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();
	private List<Employee> employees;

	public EmployeeImpl() {
        this.employees = getEmployeeList();
    }

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public Employee findEmployee(String CNP) {

		int i = 0;
		while(i < employees.size()) {
			if(employees.get(i).getCnp().equals(CNP)) {
				return employees.get(i);
			} else {
				i++;
			}
		}
		return null;
	}

	@Override
	public void deleteEmployee(Employee employee) {

		int i = 0;

		while(i < employees.size()) {
			if(employees.get(i).getCnp().equals(employee.getCnp())) {
				employees.remove(employees.get(i));
			} else {
				i++;
			}
		}


		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
			bw.write("");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
			for (Employee e: employees) {
				bw.write(e.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		// TODO Auto-generated method stub
		deleteEmployee(oldEmployee);
		addEmployee(newEmployee);
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}


	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		List<Employee> employeeList = getEmployeeList();

		if (criteria.equals("Salary")) {
			Collections.sort(employeeList, new Comparator<Employee>(){
				public int compare(Employee o1, Employee o2){
					if(o1.getSalary().equals(o2.getSalary()))
						return 0;
					return o2.getSalary().compareTo(o1.getSalary());
				}
			});
		} else {
			if (criteria.equals("CNP")) {
				Collections.sort(employeeList, new Comparator<Employee>(){
					public int compare(Employee o1, Employee o2){
						String date1=o1.getCnp().substring(1,7);
						String date2=o2.getCnp().substring(1,7);
						if(date1.equals(date2)) {
							return 0;
						}
						return date1.compareTo(date2);
					}
				});
			}
		}

		return employeeList;
	}

	@Override
	public void clearRepo() {
		employees.clear();
	}
}
